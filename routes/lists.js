const express = require('express');
const router = express.Router();
const db = require('../helpers/database');

/**
 * GET /lists
 *
 * If successful, this endpoint should return an array of all Lists with HTTP
 * status code 200.
 */
router.get('/', async (req, res, next) => {
  // TODO: implement
  
  res.status(200).json({
    data: []
  });
});

module.exports = router;
