const express = require('express');
const router = express.Router();
const db = require('../helpers/database');

/**
 * POST /lists/:id/items
 *
 * This endpoint should create a List Item for the specified List. The List Item
 * should be included in the request body, and returned to the API user in the
 * response body with a 201 status code.
 */
router.post('/:id/items', async (req, res, next) => {
  const listId = req.params.id;
  const content = req.body.text;

  // todo: check listId belongs to a valid List
  // todo: sanitise content which may include striping HTML and preventing SQL attacks (not sure if the library handles this)
  // todo: check List belongs to current user

  db.any('INSERT INTO ListItems(list_id, text) VALUES($1, $2)', [listId, content]).then(createdlistItem => {
    res.status(201).json({
      listItem: { 
        id: createdlistItem.id, 
        text: createdlistItem.text 
      }
    });
  }).catch(err => {
    next(err);
  });
  
});

module.exports = router;
