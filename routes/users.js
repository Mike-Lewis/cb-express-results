const express = require('express');
const router = express.Router();
const db = require('../helpers/database');

router.get('/:id', async function (req, res, next) {
  try {
    const { id } = req.params;

    const users = await getUsers(id);
    const lists = await getLists(id);
    const userLists = combineUsersAndLists(users, lists);

    res.status(200).json({
      data: userLists
    });
  } catch (error) {
    next(error);
  }
});

/**
 * Retrieve the user matching the id
 */
function getUsers(id) {
  return db.any(`
    SELECT id, name
    FROM users
    WHERE id = $1
  `, [
    id
  ]);
}

/**
 * Retrieve all lists from the DB for the users listed
 */
function getLists(id) {
  return db.any(`
    SELECT id, title, user_id
    FROM lists
    WHERE user_id = $1
    ORDER BY id DESC
  `, [
    id
  ]);
}

/**
 * Merge the users and the lists to return an array of users
 * with id, name and newestListTitle
 */
function combineUsersAndLists(users, lists) {
  const userLists = [];
  users.forEach(user => {
    
    const newestList = lists.find(list => list.user_id === user.id);
    userLists.push({
      id: user.id,
      name: user.name,
      newestListTitle: newestList.title // if no list found then newestList.title will not be defined
    })
  });
  return userLists;
}

module.exports = router;
