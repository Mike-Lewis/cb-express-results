// Connection
const pgp = require('pg-promise')();
const cn = {
  host: '35.244.122.153',
  user: 'list-db-admin',
  password: process.env.DB_PASSWORD,
  database: 'postgres'
};

const types = pgp.pg.types;

// Parse NUMERIC to JS float
types.setTypeParser(1700, parseFloat);

module.exports = pgp(cn);
